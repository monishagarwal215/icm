package com.icm.constants;

/**
 * Created by jive on 5/15/2017.
 */

public interface ApiConstants {

    int 				REQUEST_CLAIM		 				= 10008;

    String				NIGHT_ADVISOR_PREFS					= "look_at_me";


    int 	REQUEST_GET_STORES 		                        = 1;
    int 	REQUEST_UPLOAD_PHOTO         	                = 2;
    int 	REQUEST_GET_PHOTO 		                        = 3;

    String  BASE_URL                                        =  "http://54.169.217.116";
    String  LOGO_URL                                        =  BASE_URL + "/assets/images/stores/";
    String 	URL_GET_STORES	 			                    =  BASE_URL + "/stores/get";
    String 	URL_GET_PHOTO	 			                    =  BASE_URL + "/Photos/Get";
    String 	URL_SAVE_PHOTO	 		                        =  BASE_URL + "/Photos/Save";

}

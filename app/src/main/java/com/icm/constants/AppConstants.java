package com.icm.constants;

/**
 * Created by jive on 5/15/2017.
 */

public interface AppConstants {

    int					REQUEST_CODE_CAMERA					= 10001;
    int					REQUEST_PICK_IMAGE					= 10002;
    int					REQUEST_CAMERA_PERM					= 10003;
    int					REQUEST_READ_PERM					= 10004;
    int					REQUEST_WRITE_PERM					= 10005;

    String				NIGHT_ADVISOR_PREFS					= "look_at_me";

    int                 REQUEST_ACCOUNT                     = 1001;

    String 				EXTRA_CATEGORY_ID 				    = "extra_category_id";
    String 				EXTRA_CATEGORY_NAME 				= "extra_category_name";
    String 				EXTRA_STORE_LIST         			= "extra_store_list";
    String 				EXTRA_STORE         				= "extra_store";
    String 				EXTRA_POSITION 						= "extra_position";
    String 				EXTRA_CAMERA_PATH 					= "extra_camera_path";
    String 				EXTRA_GALLERY_PATH 					= "extra_gallery_path";

    String              DB_NAME                             = "icm";
}

package com.icm.constants;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.icm.dao.CategoryDao;
import com.icm.dao.DeviceDao;
import com.icm.dao.StoreDao;
import com.icm.model.request.Category;
import com.icm.model.request.Store;
import com.icm.pojo.Device;

/**
 * Created by monish on 06/06/17.
 */
@Database(entities = {Device.class, Category.class, Store.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {
    public abstract DeviceDao responseDao();

    public abstract CategoryDao categoryDao();

    public abstract StoreDao storeDao();
}

package com.icm.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.icm.model.request.Category;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.IGNORE;


/**
 * Created by monish on 06/06/17.
 */

@Dao
public interface CategoryDao {

    @Insert(onConflict = IGNORE)
    void insertAll(Category... category);

    @Insert(onConflict = IGNORE)
    void insertAll(List<Category> categories);

    @Query("SELECT * FROM category")
    LiveData<List<Category>> fndAll();


}

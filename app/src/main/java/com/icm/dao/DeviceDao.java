package com.icm.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;

import com.icm.pojo.Device;

import static android.arch.persistence.room.OnConflictStrategy.IGNORE;


/**
 * Created by monish on 06/06/17.
 */

@Dao
public interface DeviceDao {

    @Insert(onConflict = IGNORE)
    void insertAll(Device... devices);


}

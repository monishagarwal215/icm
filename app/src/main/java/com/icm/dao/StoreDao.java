package com.icm.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.icm.model.request.Category;
import com.icm.model.request.Store;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.IGNORE;


/**
 * Created by monish on 06/06/17.
 */

@Dao
public interface StoreDao {

    @Insert(onConflict = IGNORE)
    void insertAll(Store... category);

    @Insert(onConflict = IGNORE)
    void insertAll(List<Store> stores);

    @Query("SELECT * FROM store")
    LiveData<List<Store>> fndAll();

    @Query("SELECT * FROM store WHERE id = :id")
    Store findById(int id);

    /*@Query("SELECT * FROM store WHERE name LIKE '%:name%'")
    List<Store> findStoresByName(String name);*/

    @Update
    void upadateVisited(Store store);

    @Update
    void upadateBought(Store store);

    @Update
    void upadateRating(Store store);

    @Update
    void upadateNote(Store store);


    @Query("SELECT * FROM store WHERE bought = 1")
    List<Store> findByBought();

    @Query("SELECT * FROM store WHERE visited = 1")
    List<Store> findByVisited();
}

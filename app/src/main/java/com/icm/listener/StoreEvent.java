package com.icm.listener;

import com.icm.model.request.Store;

/**
 * Created by monish on 05/06/17.
 */

public class StoreEvent {
    private Store store;

    public StoreEvent(Store store) {
        this.store = store;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }
}

package com.icm.model.request;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by monish on 18/06/17.
 */

@Entity(tableName = "category")
public class Category {
    @PrimaryKey
    @SerializedName("category_id")
    private int id;
    @SerializedName("category_name")
    private String name;

    @Ignore
    private List<Store> stores;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Store> getStores() {
        return stores;
    }

    public void setStores(List<Store> stores) {
        this.stores = stores;
    }
}

package com.icm.model.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by monish on 18/06/17.
 */

public class CategoryResponse {
    @SerializedName("item")
    private List<Category> categoryList;

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }
}

package com.icm.model.request;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.os.Parcel;
import android.os.Parcelable;

import com.icm.util.ArrayConverter;

import java.util.List;

/**
 * Created by monish on 18/06/17.
 */

@Entity(tableName = "store")
public class Store implements Parcelable {
    @PrimaryKey
    private int id;
    private String name;
    private String participant;
    private String logo;
    private String description;
    private String location;
    @TypeConverters(ArrayConverter.class)
    private List<String> telephone;
    @TypeConverters(ArrayConverter.class)
    private List<String> email;
    private int rating;
    private String notes;
    private boolean bought;
    private boolean visited;

    public Store(Parcel in) {
        this.bought = in.readByte() == 1;
        this.visited = in.readByte() == 1;
        this.id = in.readInt();
        this.name = in.readString();
        this.participant = in.readString();
        this.logo = in.readString();
        this.description = in.readString();
        this.location = in.readString();
        in.readStringList(this.telephone);
        in.readStringList(this.email);

    }

    public Store() {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (bought ? 1 : 0));
        dest.writeByte((byte) (visited ? 1 : 0));
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(participant);
        dest.writeString(logo);
        dest.writeString(description);
        dest.writeString(location);
        dest.writeStringList(telephone);
        dest.writeStringList(email);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParticipant() {
        return participant;
    }

    public void setParticipant(String participant) {
        this.participant = participant;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<String> getTelephone() {
        return telephone;
    }

    public void setTelephone(List<String> telephone) {
        this.telephone = telephone;
    }

    public List<String> getEmail() {
        return email;
    }

    public void setEmail(List<String> email) {
        this.email = email;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public boolean isBought() {
        return bought;
    }

    public void setBought(boolean bought) {
        this.bought = bought;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Store store = (Store) o;

        return id == store.id;

    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<Store> CREATOR = new Parcelable.Creator<Store>() {

        public Store createFromParcel(Parcel in) {
            return new Store(in);
        }

        public Store[] newArray(int size) {
            return new Store[size];
        }
    };
}

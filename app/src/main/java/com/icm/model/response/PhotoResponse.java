package com.icm.model.response;

import java.util.List;

/**
 * Created by monish on 26/06/17.
 */

public class PhotoResponse {
    private List<Photos> photos;

    public List<Photos> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photos> photos) {
        this.photos = photos;
    }
}

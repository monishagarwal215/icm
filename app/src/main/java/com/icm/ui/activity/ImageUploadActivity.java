package com.icm.ui.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.icm.R;
import com.icm.constants.ApiConstants;
import com.icm.constants.AppConstants;
import com.icm.listener.UploadImageEvent;
import com.icm.model.response.UploadResponse;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyMultipartRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.BitmapUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;

/**
 * Created by monish on 26/06/17.
 */

public class ImageUploadActivity extends BaseActivity {

    private File file;
    private Bitmap mImageBitmap;
    private ImageView imgPhoto;
    private EditText edtCaption;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_upload);

        imgPhoto = (ImageView) findViewById(R.id.imgPhoto);
        edtCaption = (EditText) findViewById(R.id.edtCaption);

        Intent intent = getIntent();
        if (intent != null) {
            if (!StringUtils.isNullOrEmpty(intent.getStringExtra(AppConstants.EXTRA_GALLERY_PATH))) {
                try {
                    setGalleryImage(intent.getStringExtra(AppConstants.EXTRA_GALLERY_PATH));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (!StringUtils.isNullOrEmpty(intent.getStringExtra(AppConstants.EXTRA_CAMERA_PATH))) {
                try {
                    setCameraImage(intent.getStringExtra(AppConstants.EXTRA_CAMERA_PATH));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        findViewById(R.id.txtDone).setOnClickListener(this);
    }

    private void setCameraImage(String cameraPath) throws IOException {
        int maxImageSize = BitmapUtils.getMaxSize(this);
        file = new File(cameraPath);
        Bitmap sourceBitmap = BitmapUtils.getScaledBitmap(file, maxImageSize);
        Matrix matrix = getOrientedImage(cameraPath);
        mImageBitmap = Bitmap.createBitmap(sourceBitmap, 0, 0, sourceBitmap.getWidth(), sourceBitmap.getHeight(), matrix, true);

        ((ImageView) findViewById(R.id.imgPhoto)).setImageBitmap(mImageBitmap);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        mImageBitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);

        //hitApiRequest(ApiConstants.REQUEST_UPLOAD_PHOTO, false);
    }

    private void setGalleryImage(String galleryPath) throws URISyntaxException, IOException {
        Log.e("File", "filePath: " + galleryPath);
        File file = new File(new URI("file://" + galleryPath.replace(" ", "%20")));
        int maxImageSize = BitmapUtils.getMaxSize(this);
        Bitmap sourceBitmap = BitmapUtils.getScaledBitmap(file, maxImageSize);

        Matrix matrix = getOrientedImage(file.getPath());

        mImageBitmap = Bitmap.createBitmap(sourceBitmap, 0, 0, sourceBitmap.getWidth(), sourceBitmap.getHeight(), matrix, true);

        ((ImageView) findViewById(R.id.imgPhoto)).setImageBitmap(mImageBitmap);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        mImageBitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);

        //hitApiRequest(ApiConstants.REQUEST_UPLOAD_PHOTO, false);

    }

    private Matrix getOrientedImage(String path) throws IOException {
        ExifInterface exif = new ExifInterface(path);
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        Log.e("Capture image", "oreination" + orientation);
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.postRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.postRotate(180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.postRotate(270);
                break;
        }
        return matrix;
    }

    public void hitApiRequest(int reqType, boolean showLoader) {
        if (this == null) {
            return;
        }
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        Class className;
        if (showLoader) {
            ((BaseActivity) this).showProgressDialog();
        }
        switch (reqType) {
            case ApiConstants.REQUEST_UPLOAD_PHOTO:
                url = ApiConstants.URL_SAVE_PHOTO;
                className = UploadResponse.class;
                VolleyMultipartRequest request = new VolleyMultipartRequest(url, new UpdateListener(this, this, reqType, className), getProfileImage(), getParams(reqType));
                ((BaseApplication) getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }

    private File getProfileImage() {
        if (mImageBitmap == null) {
            mImageBitmap = ((BitmapDrawable) imgPhoto.getDrawable()).getBitmap();
            if (mImageBitmap == null) {
                return null;
            }
        }
        // create a file to write bitmap data
        File f = new File(getCacheDir().getAbsolutePath(), "profileImage.jpeg");

        // Convert bitmap to byte array
        FileOutputStream fout;
        try {
            fout = new FileOutputStream(f);
            mImageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fout);
            fout.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return f;
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (reqType == ApiConstants.REQUEST_UPLOAD_PHOTO) {
            params.put("caption", edtCaption.getText().toString());
        }
        return params;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            removeProgressDialog();
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_UPLOAD_PHOTO:
                    UploadResponse uploadResponse = (UploadResponse) responseObject;
                    if (uploadResponse.getStatus() == 1) {
                        ToastUtils.showToast(this, uploadResponse.getMessage());
                        //EventBus.getDefault().post(new UploadImageEvent());
                        AlertDialogUtils.showAlertDialog(this, "IMC", "Image Uploaded Successfully", new AlertDialogUtils.OnButtonClickListener() {
                            @Override
                            public void onButtonClick(int buttonId) {
                                finish();
                            }
                        });
                    } else {
                        ToastUtils.showToast(this, uploadResponse.getMessage());
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtDone:
                if (validate()) {
                    hitApiRequest(ApiConstants.REQUEST_UPLOAD_PHOTO, true);
                }
                break;
            default:
                super.onClick(v);
        }
    }

    private boolean validate() {
        if (mImageBitmap == null) {
            ToastUtils.showToast(this, "No image is added");
            return false;
        }
        /*if (StringUtils.isNullOrEmpty(edtCaption.getText().toString())) {
            ToastUtils.showToast(this, "Please add caption to an image");
            return false;
        }*/
        return true;
    }
}

package com.icm.ui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;

import com.icm.R;
import com.icm.ui.adapter.HomePagerAdapter;
import com.icm.util.CustomViewPager;
import com.krapps.ui.BaseActivity;

public class MainActivity extends BaseActivity implements TabLayout.OnTabSelectedListener {

    private TabLayout slidingTabLayout;
    private CustomViewPager viewPager;
    private HomePagerAdapter homePagerAdapter;
    private boolean doubleBackToExitPressedOnce;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUpSlidingTabLayout();
    }

    private void setUpSlidingTabLayout() {
        viewPager = (CustomViewPager) findViewById(R.id.viewpager);
        viewPager.setPagingEnabled(false);
        homePagerAdapter = new HomePagerAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(homePagerAdapter);

        slidingTabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        slidingTabLayout.setupWithViewPager(viewPager);
        slidingTabLayout.setOnTabSelectedListener(this);
        // Iterate over all tabs and set the custom view
        for (int i = 0; i < slidingTabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = slidingTabLayout.getTabAt(i);
            tab.setCustomView(homePagerAdapter.getTabView(i));
        }
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(4);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    /*@Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Snackbar.make(findViewById(R.id.rtlParent), R.string.snackbar_text, Snackbar.LENGTH_LONG)
                .show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }*/
}

package com.icm.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.icm.R;
import com.krapps.ui.BaseActivity;


public class SplashActivity extends BaseActivity {
    private String TAG = SplashActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);
        startNextActivity();

    }


    protected void startNextActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, 1000);
    }
}

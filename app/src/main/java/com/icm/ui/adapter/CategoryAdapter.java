package com.icm.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.icm.R;
import com.icm.model.request.Category;
import com.icm.ui.fragment.CategoryFragment;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    private Context mContext;
    private LayoutInflater mInflator;
    private List<Category> mlistData;
    private List<Category> filterList;
    private OnClickListener mClickListener;

    public CategoryAdapter(Context context, CategoryFragment categoryFragment) {
        mContext = context;
        mClickListener = categoryFragment;
        filterList = new ArrayList<>();
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public List<Category> getListData() {
        return mlistData;
    }

    public void setListData(List<Category> categoryList) {
        this.mlistData = categoryList;
        filterList.addAll(categoryList);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_category, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewholder, int position) {


        if (!StringUtils.isNullOrEmpty(filterList.get(position).getName())) {
            viewholder.txtCategotyName.setText(filterList.get(position).getName());
        } else {
            viewholder.txtCategotyName.setText("");
        }

        viewholder.rtlParent.setTag(filterList.get(position));
        viewholder.rtlParent.setOnClickListener(mClickListener);


    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return filterList == null ? 0 : filterList.size();
    }

    public void filter(final String text) {
        new Thread(new Runnable() {
            @Override
            public void run() {

                filterList.clear();
                if (TextUtils.isEmpty(text)) {
                    filterList.addAll(mlistData);
                } else {
                    for (Category item : mlistData) {
                        if (Pattern.compile(Pattern.quote(text), Pattern.CASE_INSENSITIVE).matcher(item.getName()).find()) {
                            filterList.add(item);
                        }
                    }
                }

                ((BaseActivity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        notifyDataSetChanged();
                    }
                });
            }
        }).start();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtCategotyName;
        RelativeLayout rtlParent;

        public ViewHolder(View itemView) {
            super(itemView);

            rtlParent = (RelativeLayout) itemView.findViewById(R.id.rtlParent);
            txtCategotyName = (TextView) itemView.findViewById(R.id.txtCategotyName);
        }
    }


}

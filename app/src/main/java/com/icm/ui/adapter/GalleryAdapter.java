package com.icm.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.icm.R;
import com.icm.model.response.Photos;
import com.icm.ui.fragment.GalleryFragment;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.StringUtils;

import java.util.List;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    private final int screenWidth;
    private final int screenHeight;
    private final float density;
    private Context mContext;
    private LayoutInflater mInflator;
    private List<Photos> mlistdata;
    private OnClickListener mClickListener;

    public GalleryAdapter(Context context, GalleryFragment galleryFragment) {
        mContext = context;
        mClickListener = galleryFragment;
        screenWidth = mContext.getResources().getDisplayMetrics().widthPixels;
        screenHeight = mContext.getResources().getDisplayMetrics().heightPixels;
        density = mContext.getResources().getDisplayMetrics().density;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public void setListData(List<Photos> photosList) {
        this.mlistdata = photosList;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }


    @Override
    public int getItemCount() {
        return mlistdata == null ? 0 : mlistdata.size();
    }


    @Override
    public GalleryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_gallery, parent, false);
        return new GalleryAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(GalleryAdapter.ViewHolder viewholder, final int position) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) viewholder.imgPhoto.getLayoutParams();
        layoutParams.height = (int) (screenWidth - (40 * density)) / 3;
        viewholder.imgPhoto.setLayoutParams(layoutParams);

        if (position == 0) {
            viewholder.imgAddPhoto.setVisibility(View.VISIBLE);
            viewholder.txtText.setVisibility(View.GONE);
        } else {
            viewholder.imgAddPhoto.setVisibility(View.GONE);
            viewholder.txtText.setVisibility(View.VISIBLE);

            if (mlistdata.get(position).getImage() != null) {
                ((BaseActivity) mContext).loadParseFileInBackground(mlistdata.get(position).getImage(), viewholder.imgPhoto);
            } else {
                //viewholder.imgPhoto.setBackgroundColor(mContext.getResources().getColor(R.color.grey_line));
            }
        }


        if (!StringUtils.isNullOrEmpty(mlistdata.get(position).getCaption())) {
            viewholder.txtText.setText(mlistdata.get(position).getCaption());
        } else {
            viewholder.txtText.setText("");
        }

        if (position == 0) {
            viewholder.rtlItem.setTag(mlistdata.get(position));
            viewholder.rtlItem.setOnClickListener(mClickListener);
        } else {
            viewholder.rtlItem.setOnClickListener(null);
        }

    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtText;
        ImageView imgPhoto;
        ImageView imgAddPhoto;
        RelativeLayout rtlItem;

        public ViewHolder(View itemView) {
            super(itemView);
            rtlItem = (RelativeLayout) itemView.findViewById(R.id.rtlItem);
            txtText = (TextView) itemView.findViewById(R.id.txtText);
            imgPhoto = (ImageView) itemView.findViewById(R.id.imgPhoto);
            imgAddPhoto = (ImageView) itemView.findViewById(R.id.imgAddPhoto);
        }
    }
}

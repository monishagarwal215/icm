package com.icm.ui.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.icm.R;
import com.icm.ui.fragment.GalleryFragment;
import com.icm.ui.fragment.HomeFragment;
import com.icm.ui.fragment.LayoutFragment;
import com.icm.ui.fragment.MeFragment;
import com.icm.ui.fragment.StallsFragment;

/**
 * Created by Monish on 2/2/2016.
 */
public class HomePagerAdapter extends FragmentStatePagerAdapter {

    final static String LOG_TAG = "HomePagerAdapter";

    private Context mContext;
    private HomeFragment homeFragment;
    private LayoutFragment layoutFragment;
    private StallsFragment stallsFragment;
    private MeFragment meFragment;
    private GalleryFragment galleryFragment;

    private String title[] = {"HOME", "LAYOUT", "STALLS", "ME", "GALLERY"};
    private int titleIcon[] = {R.drawable.home_tab_selected,
            R.drawable.layout_tab_selected,
            R.drawable.stalls_tab_selected,
            R.drawable.me_tab_selected,
            R.drawable.gallery_tab_selected};

    public HomePagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            if (homeFragment == null) {
                homeFragment = new HomeFragment();
            }
            return homeFragment;
        } else if (position == 1) {
            if (layoutFragment == null) {
                layoutFragment = new LayoutFragment();
            }
            return layoutFragment;
        } else if (position == 2) {
            if (stallsFragment == null) {
                stallsFragment = new StallsFragment();
            }
            return stallsFragment;
        } else if (position == 3) {
            if (meFragment == null) {
                meFragment = new MeFragment();
            }
            return meFragment;
        } else if (position == 4) {
            if (galleryFragment == null) {
                galleryFragment = new GalleryFragment();
            }
            return galleryFragment;
        }
        return homeFragment;
    }


    public int getItemPosition(Object object) {
        // hack to update adapter on notify data set changes
        return POSITION_NONE;
    }


    public View getTabView(int position) {
        View v = null;
        v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab, null);
        TextView tv = (TextView) v.findViewById(R.id.textView);
        tv.setText(title[position]);
        ImageView img = (ImageView) v.findViewById(R.id.imgView);
        img.setImageResource(titleIcon[position]);
        return v;
    }


    @Override
    public int getCount() {
        return 5;
    }

}

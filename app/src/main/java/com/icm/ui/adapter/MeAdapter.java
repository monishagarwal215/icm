package com.icm.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.icm.R;
import com.icm.model.request.Members;
import com.icm.model.request.Store;
import com.icm.ui.fragment.MeFragment;
import com.icm.ui.fragment.VisitedBoughtFragment;
import com.krapps.utils.StringUtils;

import java.util.List;

public class MeAdapter extends RecyclerView.Adapter<MeAdapter.ViewHolder> {

    private Context mContext;
    private LayoutInflater mInflator;
    private List<Store> mlistData;
    private OnClickListener mClickListener;

    public MeAdapter(Context context, VisitedBoughtFragment meFragment) {
        mContext = context;
        mClickListener = meFragment;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListData(List<Store> storeList) {
        this.mlistData = storeList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_me, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewholder, int position) {


        if (!StringUtils.isNullOrEmpty(mlistData.get(position).getName())) {
            viewholder.txtStoreName.setText(mlistData.get(position).getName());
        } else {
            viewholder.txtStoreName.setText("");
        }

        viewholder.txtRating.setText(mlistData.get(position).getRating() + "");

        viewholder.rtlParent.setTag(mlistData.get(position));

        viewholder.rtlParent.setOnClickListener(mClickListener);

        switch (mlistData.get(position).getRating()) {
            case 1:
                viewholder.txtRating.setBackgroundColor(mContext.getResources().getColor(R.color.blue_1));
                break;
            case 2:
                viewholder.txtRating.setBackgroundColor(mContext.getResources().getColor(R.color.blue_2));
                break;
            case 3:
                viewholder.txtRating.setBackgroundColor(mContext.getResources().getColor(R.color.blue_3));
                break;
            case 4:
                viewholder.txtRating.setBackgroundColor(mContext.getResources().getColor(R.color.blue_4));
                break;
            case 5:
                viewholder.txtRating.setBackgroundColor(mContext.getResources().getColor(R.color.blue_5));
                break;
        }

    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mlistData == null ? 0 : mlistData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtRating;
        TextView txtStoreName;
        RelativeLayout rtlParent;

        public ViewHolder(View itemView) {
            super(itemView);

            rtlParent = (RelativeLayout) itemView.findViewById(R.id.rtlParent);
            txtRating = (TextView) itemView.findViewById(R.id.txtRating);
            txtStoreName = (TextView) itemView.findViewById(R.id.txtStoreName);
        }
    }


}

package com.icm.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.icm.R;
import com.icm.model.request.Members;
import com.krapps.utils.BitmapUtils;
import com.krapps.utils.StringUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MembersAdapter extends RecyclerView.Adapter<MembersAdapter.ViewHolder> {

    private Context mContext;
    private LayoutInflater mInflator;
    private List<Members> mlistData;
    private OnClickListener mClickListener;
    private Picasso picasso;
    private float density;

    public MembersAdapter(Context context) {
        mContext = context;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        picasso = Picasso.with(mContext);
        density = mContext.getResources().getDisplayMetrics().density;
    }

    public void setListData(List<Members> membersList) {
        this.mlistData = membersList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_members, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewholder, int position) {
        if (!StringUtils.isNullOrEmpty(mlistData.get(position).getName())) {
            viewholder.txtName.setText(mlistData.get(position).getName());
        } else {
            viewholder.txtName.setText("");
        }

        if (!StringUtils.isNullOrEmpty(mlistData.get(position).getDescription())) {
            viewholder.txtDescription.setText(mlistData.get(position).getDescription());
        } else {
            viewholder.txtDescription.setText("");
        }

        if (!StringUtils.isNullOrEmpty(mlistData.get(position).getPostType())) {
            viewholder.txtPost.setText(mlistData.get(position).getPostType());
        } else {
            viewholder.txtPost.setText("");
        }

        /*try {
            picasso.load(mContext.getResources().getIdentifier(
                    mlistData.get(position).getPhotoId(), "drawable", mContext.getPackageName()))
                    //.resize((int) (100 * density), (int) (135 * density))
                    .into(viewholder.imgMember);
        } catch (Exception e) {
            Log.d("pos", position + "");
        }*/

        try {
            /*viewholder.imgMember.setImageResource(mContext.getResources().getIdentifier(
                    mlistData.get(position).getPhotoId(), "drawable", mContext.getPackageName()));*/
            viewholder.imgMember.setImageBitmap(BitmapUtils.decodeSampledBitmapFromResource(mContext.getResources(), mContext.getResources().getIdentifier(
                    mlistData.get(position).getPhotoId(), "drawable", mContext.getPackageName()), (int) (100 * density), (int) (135 * density)));
        } catch (Exception e) {
            Log.d("pos", position + "");
        }
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mlistData == null ? 0 : mlistData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtPost;
        TextView txtName;
        TextView txtDescription;
        ImageView imgMember;

        public ViewHolder(View itemView) {
            super(itemView);

            txtPost = (TextView) itemView.findViewById(R.id.txtPost);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtDescription = (TextView) itemView.findViewById(R.id.txtDescription);

            imgMember = (ImageView) itemView.findViewById(R.id.imgMember);
        }
    }


}

package com.icm.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.icm.R;
import com.icm.model.request.Store;
import com.icm.ui.fragment.StoreListingFragment;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class StoreAdapter extends RecyclerView.Adapter<StoreAdapter.ViewHolder> {

    private Context mContext;
    private LayoutInflater mInflator;
    private List<Store> mlistData;
    private List<Store> filterList;
    private OnClickListener mClickListener;

    public StoreAdapter(Context context, StoreListingFragment storeListingFragment) {
        mContext = context;
        mClickListener = storeListingFragment;
        filterList = new ArrayList<>();
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListData(List<Store> storeList) {
        this.mlistData = storeList;
        this.filterList.addAll(mlistData);
    }

    public List<Store> getListData() {
        return filterList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_store, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewholder, int position) {

        if (!StringUtils.isNullOrEmpty(filterList.get(position).getName())) {
            viewholder.txtStoreName.setText(filterList.get(position).getName());
        } else {
            viewholder.txtStoreName.setText("");
        }

        if (filterList.get(position).isBought()) {
            viewholder.imgBought.setImageResource(R.drawable.bought_active);
        } else {
            viewholder.imgBought.setImageResource(R.drawable.bought_inactive);
        }

        if (filterList.get(position).isVisited()) {
            viewholder.imgVisited.setImageResource(R.drawable.visited_active);
        } else {
            viewholder.imgVisited.setImageResource(R.drawable.visited_inactive);
        }

        viewholder.txtRating.setText(filterList.get(position).getRating() + "");

        viewholder.rtlParent.setTag(filterList.get(position));

        viewholder.rtlParent.setOnClickListener(mClickListener);


        switch (filterList.get(position).getRating()) {
            case 1:
                viewholder.txtRating.setBackgroundColor(mContext.getResources().getColor(R.color.blue_1));
                break;
            case 2:
                viewholder.txtRating.setBackgroundColor(mContext.getResources().getColor(R.color.blue_2));
                break;
            case 3:
                viewholder.txtRating.setBackgroundColor(mContext.getResources().getColor(R.color.blue_3));
                break;
            case 4:
                viewholder.txtRating.setBackgroundColor(mContext.getResources().getColor(R.color.blue_4));
                break;
            case 5:
                viewholder.txtRating.setBackgroundColor(mContext.getResources().getColor(R.color.blue_5));
                break;
        }

    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return filterList == null ? 0 : filterList.size();
    }


    public void filter(final String text) {
        new Thread(new Runnable() {
            @Override
            public void run() {

                filterList.clear();
                if (TextUtils.isEmpty(text)) {
                    filterList.addAll(mlistData);
                } else {
                    for (Store item : mlistData) {
                        if (Pattern.compile(Pattern.quote(text), Pattern.CASE_INSENSITIVE).matcher(item.getName()).find()) {
                            filterList.add(item);
                        }
                    }
                }

                ((BaseActivity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        notifyDataSetChanged();
                    }
                });
            }
        }).start();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtRating;
        TextView txtStoreName;
        ImageView imgBought;
        ImageView imgVisited;
        RelativeLayout rtlParent;

        public ViewHolder(View itemView) {
            super(itemView);

            rtlParent = (RelativeLayout) itemView.findViewById(R.id.rtlParent);
            imgBought = (ImageView) itemView.findViewById(R.id.imgBought);
            imgVisited = (ImageView) itemView.findViewById(R.id.imgVisited);
            txtRating = (TextView) itemView.findViewById(R.id.txtRating);
            txtStoreName = (TextView) itemView.findViewById(R.id.txtStoreName);
        }
    }
}

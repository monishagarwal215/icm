package com.icm.ui.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.icm.R;
import com.icm.constants.ApiConstants;
import com.icm.constants.AppConstants;
import com.icm.constants.AppDatabase;
import com.icm.model.request.Category;
import com.icm.model.request.CategoryResponse;
import com.icm.model.request.Store;
import com.icm.ui.adapter.CategoryAdapter;
import com.icm.util.LookAtMePrefernece;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;


/**
 * Created by Monish Agarwal on 2/2/2016.
 */
public class CategoryFragment extends BaseFragment implements TextWatcher {

    private String TAG = CategoryFragment.class.getSimpleName();
    private AppDatabase appDatabase;
    private View mView;
    private String url;
    private EditText edtSearch;
    private CategoryAdapter categoryAdapter;
    private boolean searchInProgress;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_category, null);

        edtSearch = (EditText) mView.findViewById(R.id.edtSearch);
        edtSearch.addTextChangedListener(this);

        if (LookAtMePrefernece.getInstance().getCategories() != null) {
            setupAdapter(LookAtMePrefernece.getInstance().getCategories());
            hitApiRequest(ApiConstants.REQUEST_GET_STORES, false);
        } else {
            hitApiRequest(ApiConstants.REQUEST_GET_STORES, true);
            /*String json = getCategoryJson();
            Gson gson = new Gson();
            CategoryResponse categoryResponse = gson.fromJson(json, CategoryResponse.class);
            if (categoryResponse != null && categoryResponse.getCategoryList() != null) {
                LookAtMePrefernece.getInstance().saveCategories(categoryResponse.getCategoryList());
                setupAdapter(categoryResponse.getCategoryList());
            } else {

            }*/
        }

        return mView;
    }

    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        if (showLoader) {
            ((BaseActivity) getActivity()).showProgressDialog();
        }
        switch (reqType) {
            case ApiConstants.REQUEST_GET_STORES:
                url = String.format(ApiConstants.URL_GET_STORES);
                className = CategoryResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                }, getParams(ApiConstants.REQUEST_GET_STORES));
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (reqType == ApiConstants.REQUEST_GET_STORES) {

        }
        return params;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            ((BaseActivity) getActivity()).removeProgressDialog();
            if (!isSuccess) {
                ToastUtils.showToast(getActivity(), "Some error occured.");
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_GET_STORES:
                    CategoryResponse categoryResponse = (CategoryResponse) responseObject;
                    if (categoryResponse != null && categoryResponse.getCategoryList() != null && categoryResponse.getCategoryList().size() > 0) {
                        LookAtMePrefernece.getInstance().saveCategories(categoryResponse.getCategoryList());
                        setupAdapter(categoryResponse.getCategoryList());
                    } else {
                        setupAdapter(null);
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    private String getCategoryJson() {
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(getActivity().getAssets().open("stores.json")));

            String mLine;
            while ((mLine = reader.readLine()) != null) {
                stringBuilder.append(mLine);
            }
        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
        return stringBuilder.toString();
    }


    private void searchTextInCategory(String searchableString) {
        categoryAdapter.filter(searchableString);
    }


    private void setupAdapter(List<Category> categoryList) {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        RecyclerView listview = (RecyclerView) mView.findViewById(R.id.listview);
        listview.setNestedScrollingEnabled(false);
        listview.setFocusable(false);
        listview.setLayoutManager(linearLayoutManager);

        categoryAdapter = new CategoryAdapter(getActivity(), this);
        categoryAdapter.setListData(categoryList);
        listview.setAdapter(categoryAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rtlParent:
                Category category = (Category) view.getTag();
                Bundle bundle = new Bundle();
                bundle.putInt(AppConstants.EXTRA_CATEGORY_ID, category.getId());
                bundle.putParcelableArrayList(AppConstants.EXTRA_STORE_LIST, (ArrayList<? extends Parcelable>) category.getStores());
                bundle.putString(AppConstants.EXTRA_CATEGORY_NAME, category.getName());
                ((BaseActivity) getActivity()).replaceFragment(CategoryFragment.class.getSimpleName(), new StoreListingFragment(), bundle, true, R.id.contentFrame);
                break;
            default:
                super.onClick(view);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        searchTextInCategory(s.toString());
    }

}
package com.icm.ui.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.icm.R;
import com.icm.constants.ApiConstants;
import com.icm.constants.AppConstants;
import com.icm.listener.OnButtonClicked;
import com.icm.listener.UploadImageEvent;
import com.icm.model.response.Photos;
import com.icm.model.response.PhotoResponse;
import com.icm.model.response.UploadResponse;
import com.icm.ui.activity.ImageUploadActivity;
import com.icm.ui.adapter.GalleryAdapter;
import com.icm.ui.dialog.ImageOnlyOptionsDialog;
import com.icm.util.GridSpacingItemDecoration;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.DocumentUtils;
import com.krapps.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by Monish Agarwal on 2/2/2016.
 */
public class GalleryFragment extends BaseFragment {

    private String TAG = GalleryFragment.class.getSimpleName();
    private View mView;
    private String url;
    public String photoFileName = "photo.jpg";
    private ImageView imgProfile;
    private Bitmap mImageBitmap = null;
    private File file;
    private RecyclerView gridview;
    private GalleryAdapter galleryAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_gallery, null);

        final GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
        gridview = (RecyclerView) mView.findViewById(R.id.gridview);
        gridview.setNestedScrollingEnabled(false);
        gridview.setFocusable(false);
        gridview.setLayoutManager(mLayoutManager);
        galleryAdapter = new GalleryAdapter(getActivity(), this);
        gridview.addItemDecoration(new GridSpacingItemDecoration(3, ((BaseActivity) getActivity()).dpToPx(10), true));
        gridview.setAdapter(galleryAdapter);


        hitApiRequest(ApiConstants.REQUEST_GET_PHOTO, false);
        return mView;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == AppConstants.REQUEST_CODE_CAMERA) {
            if (resultCode == getActivity().RESULT_OK) {
                try {
                    Uri takenPhotoUri = getPhotoFileUri(photoFileName);

                    Intent intent = new Intent(getActivity(), ImageUploadActivity.class);
                    intent.putExtra(AppConstants.EXTRA_CAMERA_PATH, takenPhotoUri.getPath());
                    startActivity(intent);

                    /*int maxImageSize = BitmapUtils.getMaxSize(getActivity());
                    file = new File(takenPhotoUri.getPath());
                    Bitmap sourceBitmap = BitmapUtils.getScaledBitmap(file, maxImageSize);
                    Matrix matrix = getOrientedImage(takenPhotoUri.getPath());
                    mImageBitmap = Bitmap.createBitmap(sourceBitmap, 0, 0, sourceBitmap.getWidth(), sourceBitmap.getHeight(), matrix, true);

                    ((ImageView) findViewById(R.id.imgPhoto)).setImageBitmap(mImageBitmap);

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    mImageBitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);

                    hitApiRequest(ApiConstants.REQUEST_UPLOAD, true);*/
                } catch (Exception ex) {
                    ex.printStackTrace();
                    Toast.makeText(getActivity(), "Image not supported", Toast.LENGTH_LONG).show();
                }
            }
        } else if (requestCode == AppConstants.REQUEST_PICK_IMAGE) {
            // get the returned data
            if (resultCode == getActivity().RESULT_OK) {
                try {
                    Uri selectedImage = data.getData();
                    String filePath = DocumentUtils.getPath(getActivity(), selectedImage);

                    Intent intent = new Intent(getActivity(), ImageUploadActivity.class);
                    intent.putExtra(AppConstants.EXTRA_GALLERY_PATH, filePath);
                    startActivity(intent);

                    /*Log.e("File", "filePath: " + filePath);
                    File file = new File(new URI("file://" + filePath.replace(" ", "%20")));
                    int maxImageSize = BitmapUtils.getMaxSize(this);
                    Bitmap sourceBitmap = BitmapUtils.getScaledBitmap(file, maxImageSize);

                    Matrix matrix = getOrientedImage(file.getPath());

                    mImageBitmap = Bitmap.createBitmap(sourceBitmap, 0, 0, sourceBitmap.getWidth(), sourceBitmap.getHeight(), matrix, true);

                    ((ImageView) findViewById(R.id.imgPhoto)).setImageBitmap(mImageBitmap);

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    mImageBitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);

                    hitApiRequest(ApiConstants.REQUEST_UPLOAD, true);*/
                } catch (Exception ex) {
                    ex.printStackTrace();
                    Toast.makeText(getActivity(), "Image not supported", Toast.LENGTH_LONG).show();
                }
            }
        } else if (requestCode == AppConstants.REQUEST_CAMERA_PERM) {
            Log.d("camera per", requestCode + "");
        }
    }


    @Override
    public void onResume() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onResume();
    }


    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void updateUploadedImage(UploadImageEvent uploadImageEvent) {
        hitApiRequest(ApiConstants.REQUEST_GET_PHOTO, false);
    }

    private void setupAdapter(List<Photos> photosList) {

        Photos photos = new Photos();
        photos.setId(-1);

        if (photosList == null) {
            photosList = new ArrayList<>();
        }
        photosList.add(0, photos);

        galleryAdapter.setListData(photosList);
        galleryAdapter.notifyDataSetChanged();
    }


    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        if (showLoader) {
            ((BaseActivity) getActivity()).showProgressDialog();
        }
        switch (reqType) {
            case ApiConstants.REQUEST_GET_PHOTO:
                url = String.format(ApiConstants.URL_GET_PHOTO);
                className = PhotoResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                }, getParams(ApiConstants.REQUEST_GET_PHOTO));
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (reqType == ApiConstants.REQUEST_GET_PHOTO) {

        }
        return params;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            ((BaseActivity) getActivity()).removeProgressDialog();
            if (!isSuccess) {
                ToastUtils.showToast(getActivity(), "Some error occured.");
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_GET_PHOTO:
                    PhotoResponse photoResponse = (PhotoResponse) responseObject;
                    if (photoResponse != null && photoResponse.getPhotos() != null && photoResponse.getPhotos().size() > 0) {
                        setupAdapter(photoResponse.getPhotos());
                    } else {
                        setupAdapter(null);
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rtlItem:
                showPhotoOptionsDialog();
                break;
            default:
                super.onClick(view);
        }
    }

    private void showPhotoOptionsDialog() {
        ImageOnlyOptionsDialog dialog = new ImageOnlyOptionsDialog();
        dialog.setonButtonClickListener(new OnButtonClicked() {
            @Override
            public void onButtonCLick(int buttonId) {
                switch (buttonId) {
                    case R.id.btnCamera:
                        if (checkCameraPermission() && checkReadStoragePermission() && checkWriteStoragePermission()) {
                            openCamera();
                        }
                        break;
                    case R.id.btnGallery:
                        if (checkCameraPermission() && checkReadStoragePermission() && checkWriteStoragePermission()) {
                            openGallery();
                        }
                        break;
                    default:
                        break;
                }
            }
        });
        dialog.show(getActivity().getSupportFragmentManager(), ImageOnlyOptionsDialog.class.getSimpleName());
    }

    private boolean checkReadStoragePermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, AppConstants.REQUEST_CAMERA_PERM);
            return false;
        }
    }

    private boolean checkWriteStoragePermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, AppConstants.REQUEST_WRITE_PERM);
            return false;
        }
    }

    private boolean checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, AppConstants.REQUEST_CAMERA_PERM);
            return false;
        }
    }

    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setAction(Intent.ACTION_PICK);
        photoPickerIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, AppConstants.REQUEST_PICK_IMAGE);
    }

    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (getPhotoFileUri(photoFileName) == null) {
            ToastUtils.showToast(getActivity(), "No Sd card available");
            return;
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, getPhotoFileUri(photoFileName));
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(intent, AppConstants.REQUEST_CODE_CAMERA);
        }
    }

    // Returns the Uri for a photo stored on disk given the fileName
    public Uri getPhotoFileUri(String fileName) {
        // Only continue if the SD Card is mounted
        if (isExternalStorageAvailable()) {
            // Get safe storage directory for photos
            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "ICM TAG");

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
                Log.d("ICM TAG", "failed to create directory");
            }

            // Return the file target for the photo based on filename
            return Uri.fromFile(new File(mediaStorageDir.getPath() + File.separator + fileName));
        }
        return null;
    }

    private boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        return state.equals(Environment.MEDIA_MOUNTED);
    }
}
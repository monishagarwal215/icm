package com.icm.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.icm.R;
import com.icm.model.request.Members;
import com.icm.ui.adapter.MembersAdapter;
import com.krapps.ui.BaseFragment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;


/**
 * Created by Monish Agarwal on 2/2/2016.
 */
public class HomeFragment extends BaseFragment {

    private String TAG = HomeFragment.class.getSimpleName();
    private View mView;
    private int screenWidth;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_home, null);
        screenWidth = getActivity().getResources().getDisplayMetrics().widthPixels;
        String membersString = getMembersJson();
        Gson gson = new Gson();
        List<Members> membersList = gson.fromJson(membersString, new TypeToken<List<Members>>() {
        }.getType());
        setupAdapter(membersList);

        return mView;
    }


    private void setupAdapter(List<Members> membersList) {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        RecyclerView listview = (RecyclerView) mView.findViewById(R.id.listview);
        listview.setNestedScrollingEnabled(false);
        listview.setFocusable(false);
        listview.setLayoutManager(linearLayoutManager);

        MembersAdapter membersAdapter = new MembersAdapter(getActivity());
        membersAdapter.setListData(membersList);
        listview.setAdapter(membersAdapter);
    }


    private String getMembersJson() {
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(getActivity().getAssets().open("members.json")));

            String mLine;
            while ((mLine = reader.readLine()) != null) {
                stringBuilder.append(mLine);
            }
        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
        return stringBuilder.toString();
    }
}
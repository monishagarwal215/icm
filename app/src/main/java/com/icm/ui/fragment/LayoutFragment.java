package com.icm.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.chrisbanes.photoview.PhotoView;
import com.github.chrisbanes.photoview.PhotoViewAttacher;
import com.icm.R;
import com.krapps.ui.BaseFragment;


/**
 * Created by Monish Agarwal on 2/2/2016.
 */
public class LayoutFragment extends BaseFragment {

    private String TAG = LayoutFragment.class.getSimpleName();
    private View mView;
    PhotoView imgMap;
    PhotoViewAttacher photoViewAttacher;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_layout, null);

        imgMap = (PhotoView) mView.findViewById(R.id.imgMap);

        photoViewAttacher = new PhotoViewAttacher(imgMap);
        //photoViewAttacher.update();

        return mView;
    }


}
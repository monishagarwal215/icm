package com.icm.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.icm.R;
import com.icm.constants.AppDatabase;
import com.icm.model.request.Store;
import com.icm.ui.adapter.MeAdapter;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;

import java.util.List;


/**
 * Created by Monish Agarwal on 2/2/2016.
 */
public class MeFragment extends BaseFragment {

    private String TAG = MeFragment.class.getSimpleName();
    private MeAdapter meAdapter;
    private View mView;
    private boolean VISITED = true;
    private AppDatabase appDatabase;
    private List<Store> boughtList;
    private List<Store> visitedList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_me, null);

        ((BaseActivity) getActivity()).replaceFragment(null, new VisitedBoughtFragment(), null, false, R.id.contentFrameMe);

        return mView;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            default:
                super.onClick(view);
        }
    }
}
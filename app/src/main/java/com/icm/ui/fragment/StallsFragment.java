package com.icm.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.icm.R;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;


/**
 * Created by Monish Agarwal on 2/2/2016.
 */
public class StallsFragment extends BaseFragment {

    private String TAG = StallsFragment.class.getSimpleName();
    private View mView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_stalls, null);
        ((BaseActivity) getActivity()).replaceFragment(null, new CategoryFragment(), null, false, R.id.contentFrame);
        return mView;
    }
}
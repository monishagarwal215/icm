package com.icm.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.icm.R;
import com.icm.constants.ApiConstants;
import com.icm.constants.AppConstants;
import com.icm.constants.AppDatabase;
import com.icm.listener.StoreEvent;
import com.icm.model.request.Store;
import com.krapps.application.BaseApplication;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.StringUtils;

import org.greenrobot.eventbus.EventBus;


/**
 * Created by Monish Agarwal on 2/2/2016.
 */
public class StoreDetailFragment extends BaseFragment implements TextWatcher {

    private String TAG = StoreDetailFragment.class.getSimpleName();
    private View mView;
    private Store mStore;
    private AppDatabase appDatabase;
    private EditText edtNotes;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_store_detail, null);
        appDatabase = BaseApplication.getAppDatabase(getActivity());

        Bundle bundle = getArguments();
        if (bundle != null) {
            String categoryName = bundle.getString(AppConstants.EXTRA_CATEGORY_NAME);
            mStore = bundle.getParcelable(AppConstants.EXTRA_STORE);
            updateDataToUI(categoryName, mStore);
        }

        edtNotes = (EditText) mView.findViewById(R.id.edtNotes);
        edtNotes.addTextChangedListener(this);

        mView.findViewById(R.id.star_5).setOnClickListener(this);
        mView.findViewById(R.id.star_4).setOnClickListener(this);
        mView.findViewById(R.id.star_3).setOnClickListener(this);
        mView.findViewById(R.id.star_2).setOnClickListener(this);
        mView.findViewById(R.id.star_1).setOnClickListener(this);
        mView.findViewById(R.id.rtlBought).setOnClickListener(this);
        mView.findViewById(R.id.rtlVisited).setOnClickListener(this);
        mView.findViewById(R.id.txtHeader).setOnClickListener(this);

        return mView;
    }

    private void updateDataToUI(String categoryName, Store store) {
        ((BaseActivity) getActivity()).loadParseFileWithoutCrop(ApiConstants.LOGO_URL + store.getLogo(), (ImageView) mView.findViewById(R.id.imgStorelogo));

        ((TextView) mView.findViewById(R.id.txtStoreName)).setText(store.getName());
        ((TextView) mView.findViewById(R.id.txtCategory)).setText(categoryName);
        ((TextView) mView.findViewById(R.id.txtLocation)).setText(store.getLocation() + "");
        ((TextView) mView.findViewById(R.id.txtParticipantName)).setText(store.getParticipant());
        ((TextView) mView.findViewById(R.id.txtPhoneNo)).setText(store.getTelephone().get(0));
        ((TextView) mView.findViewById(R.id.txtEmail)).setText(store.getEmail().get(0));

        if (!StringUtils.isNullOrEmpty(mStore.getNotes())) {
            ((EditText) mView.findViewById(R.id.edtNotes)).setText(store.getNotes());
            ((EditText) mView.findViewById(R.id.edtNotes)).setSelection(store.getNotes().length());
        }

        showStars(mStore.getRating());
        updateBoughtUI();
        updateVisitedUI();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.star_1:
                showStars(1);
                break;
            case R.id.star_2:
                showStars(2);
                break;
            case R.id.star_3:
                showStars(3);
                break;
            case R.id.star_4:
                showStars(4);
                break;
            case R.id.star_5:
                showStars(5);
                break;
            case R.id.rtlBought:
                if (mStore.isBought() == true) {
                    mStore.setBought(false);
                    appDatabase.storeDao().upadateBought(mStore);
                } else {
                    mStore.setBought(true);
                    appDatabase.storeDao().upadateBought(mStore);
                }
                EventBus.getDefault().post(new StoreEvent(mStore));
                updateBoughtUI();
                break;
            case R.id.rtlVisited:
                if (mStore.isVisited() == true) {
                    mStore.setVisited(false);
                    appDatabase.storeDao().upadateVisited(mStore);
                } else {
                    mStore.setVisited(true);
                    appDatabase.storeDao().upadateVisited(mStore);
                }
                EventBus.getDefault().post(new StoreEvent(mStore));
                updateVisitedUI();
                break;
            case R.id.txtHeader:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
        super.onClick(view);
    }

    private void updateVisitedUI() {
        if (mStore.isVisited()) {
            mView.findViewById(R.id.txtVisited).setBackgroundColor(getResources().getColor(R.color.blue_transparent));
            ((TextView) mView.findViewById(R.id.txtVisited)).setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.visited_active);
            mView.findViewById(R.id.checkVisited).setVisibility(View.VISIBLE);
        } else {
            mView.findViewById(R.id.txtVisited).setBackgroundColor(getResources().getColor(android.R.color.transparent));
            ((TextView) mView.findViewById(R.id.txtVisited)).setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.visited_inactive);
            mView.findViewById(R.id.checkVisited).setVisibility(View.GONE);
        }
    }

    private void updateBoughtUI() {
        if (mStore.isBought()) {
            mView.findViewById(R.id.txtBought).setBackgroundColor(getResources().getColor(R.color.blue_transparent));
            ((TextView) mView.findViewById(R.id.txtBought)).setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.bought_active);
            mView.findViewById(R.id.checkBought).setVisibility(View.VISIBLE);
        } else {
            mView.findViewById(R.id.txtBought).setBackgroundColor(getResources().getColor(android.R.color.transparent));
            ((TextView) mView.findViewById(R.id.txtBought)).setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.bought_inactive);
            mView.findViewById(R.id.checkBought).setVisibility(View.GONE);
        }
    }

    private void showStars(int rating) {
        mStore.setRating(rating);
        appDatabase.storeDao().upadateRating(mStore);
        EventBus.getDefault().post(new StoreEvent(mStore));
        ((ImageView) mView.findViewById(R.id.star_5)).setImageResource(rating > 4 ? R.drawable.star_active : R.drawable.star_inactive);
        ((ImageView) mView.findViewById(R.id.star_4)).setImageResource(rating > 3 ? R.drawable.star_active : R.drawable.star_inactive);
        ((ImageView) mView.findViewById(R.id.star_3)).setImageResource(rating > 2 ? R.drawable.star_active : R.drawable.star_inactive);
        ((ImageView) mView.findViewById(R.id.star_2)).setImageResource(rating > 1 ? R.drawable.star_active : R.drawable.star_inactive);
        ((ImageView) mView.findViewById(R.id.star_1)).setImageResource(rating > 0 ? R.drawable.star_active : R.drawable.star_inactive);
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (!StringUtils.isNullOrEmpty(s.toString())) {
            mStore.setNotes(s.toString());
            EventBus.getDefault().post(new StoreEvent(mStore));
            appDatabase.storeDao().upadateNote(mStore);

            if (mStore.isVisited() == false) {
                mStore.setVisited(true);
                appDatabase.storeDao().upadateVisited(mStore);
                EventBus.getDefault().post(new StoreEvent(mStore));
                updateVisitedUI();
            }
        }
    }
}
package com.icm.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.icm.R;
import com.icm.constants.AppConstants;
import com.icm.constants.AppDatabase;
import com.icm.listener.StoreEvent;
import com.icm.model.request.Store;
import com.icm.ui.adapter.StoreAdapter;
import com.icm.util.VerticalItemDecoration;
import com.krapps.application.BaseApplication;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.StringUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;


/**
 * Created by Monish Agarwal on 2/2/2016.
 */
public class StoreListingFragment extends BaseFragment implements TextWatcher {

    private String TAG = StoreListingFragment.class.getSimpleName();
    private View mView;
    private String categoryName;
    private AppDatabase appDatabase;
    private StoreAdapter storeAdapter;
    private boolean searchInProgress;
    private EditText edtSearch;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_store_listing, null);
        appDatabase = BaseApplication.getAppDatabase(getActivity());

        edtSearch = (EditText) mView.findViewById(R.id.edtSearch);
        edtSearch.addTextChangedListener(this);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        RecyclerView listview = (RecyclerView) mView.findViewById(R.id.listview);
        listview.setNestedScrollingEnabled(false);
        listview.setFocusable(false);
        listview.addItemDecoration(new VerticalItemDecoration((int) (2 * getResources().getDisplayMetrics().density)));
        listview.setLayoutManager(linearLayoutManager);

        storeAdapter = new StoreAdapter(getActivity(), this);
        listview.setAdapter(storeAdapter);


        Bundle bundle = getArguments();
        if (bundle != null) {
            int categoryId = bundle.getInt(AppConstants.EXTRA_CATEGORY_ID);
            categoryName = bundle.getString(AppConstants.EXTRA_CATEGORY_NAME);
            List<Store> storeList = bundle.getParcelableArrayList(AppConstants.EXTRA_STORE_LIST);
            setupAdapter(storeList);
            appDatabase.storeDao().insertAll(storeList);
            ((TextView) mView.findViewById(R.id.txtCategotyName)).setText(categoryName);
        }

        return mView;
    }

    @Override
    public void onResume() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onResume();
    }


    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void updateStore(StoreEvent storeEvent) {
        if (storeEvent != null && storeEvent.getStore() != null) {
            Store store = storeEvent.getStore();
            if (storeAdapter.getListData() != null && storeAdapter.getListData().contains(store)) {
                int pos = storeAdapter.getListData().indexOf(store);
                storeAdapter.getListData().remove(pos);
                storeAdapter.getListData().add(pos, store);
                storeAdapter.notifyDataSetChanged();
            }
        }
    }

    private void setupAdapter(List<Store> storeList) {
        if (storeList == null) {
            return;
        }
        for (int i = 0; i <= storeList.size() - 1; i++) {
            Store temp = appDatabase.storeDao().findById(storeList.get(i).getId());
            if (temp != null && temp.isVisited()) {
                storeList.get(i).setVisited(true);
            } else {
                storeList.get(i).setVisited(false);
            }

            if (temp != null && temp.isBought()) {
                storeList.get(i).setBought(true);
            } else {
                storeList.get(i).setBought(false);
            }

            if (temp != null) {
                storeList.get(i).setRating(temp.getRating());
            }

            if (temp != null && !StringUtils.isNullOrEmpty(temp.getNotes())) {
                storeList.get(i).setNotes(temp.getNotes());
            }
        }
        storeAdapter.setListData(storeList);
        storeAdapter.notifyDataSetChanged();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rtlParent:
                Store store = (Store) view.getTag();
                Bundle bundle = new Bundle();
                bundle.putParcelable(AppConstants.EXTRA_STORE, store);
                bundle.putString(AppConstants.EXTRA_CATEGORY_NAME, categoryName);
                ((BaseActivity) getActivity()).replaceFragment(StoreListingFragment.class.getSimpleName(), new StoreDetailFragment(), bundle, true, R.id.contentFrame);
                break;
            default:
                super.onClick(view);
        }
    }


    private void searchTextInStore(String searchableString) {
        storeAdapter.filter(searchableString);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        searchTextInStore(s.toString());
    }
}
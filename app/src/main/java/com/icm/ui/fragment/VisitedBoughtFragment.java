package com.icm.ui.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.icm.R;
import com.icm.constants.AppConstants;
import com.icm.constants.AppDatabase;
import com.icm.listener.StoreEvent;
import com.icm.model.request.Store;
import com.icm.ui.adapter.MeAdapter;
import com.icm.util.VerticalItemDecoration;
import com.krapps.application.BaseApplication;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;


/**
 * Created by Monish Agarwal on 2/2/2016.
 */
public class VisitedBoughtFragment extends BaseFragment {

    private String TAG = VisitedBoughtFragment.class.getSimpleName();
    private MeAdapter meAdapter;
    private View mView;
    private boolean VISITED = true;
    private AppDatabase appDatabase;
    private List<Store> boughtList;
    private List<Store> visitedList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_visited_bought, null);
        appDatabase = BaseApplication.getAppDatabase(getActivity());

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        RecyclerView listview = (RecyclerView) mView.findViewById(R.id.listview);
        listview.setNestedScrollingEnabled(false);
        listview.setFocusable(false);
        listview.addItemDecoration(new VerticalItemDecoration((int) (2 * getResources().getDisplayMetrics().density)));
        listview.setLayoutManager(linearLayoutManager);

        meAdapter = new MeAdapter(getActivity(), this);
        listview.setAdapter(meAdapter);

        boughtList = appDatabase.storeDao().findByBought();
        visitedList = appDatabase.storeDao().findByVisited();
        setupAdapter(visitedList);


        mView.findViewById(R.id.txtVisited).setOnClickListener(this);
        mView.findViewById(R.id.txtBought).setOnClickListener(this);

        return mView;
    }


    @Override
    public void onResume() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onResume();
    }


    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void updateStore(StoreEvent storeEvent) {
        if (storeEvent != null && storeEvent.getStore() != null) {
            Store store = storeEvent.getStore();
            if (store.isVisited() == true) {
                if (!visitedList.contains(store)) {
                    visitedList.add(store);
                }
            } else {
                visitedList.remove(store);
            }

            if (store.isBought() == true) {
                if (!boughtList.contains(store)) {
                    boughtList.add(store);
                }
            } else {
                boughtList.remove(store);
            }

            meAdapter.notifyDataSetChanged();
        }
    }

    private void setupAdapter(List<Store> stores) {
        if (VISITED == true) {
            meAdapter.setListData(visitedList);
        } else {
            meAdapter.setListData(boughtList);
        }
        meAdapter.notifyDataSetChanged();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtBought:
                VISITED = false;
                showBought();
                setupAdapter(boughtList);
                break;
            case R.id.txtVisited:
                VISITED = true;
                showVisited();
                setupAdapter(visitedList);
                break;
            case R.id.rtlParent:
                Store store = (Store) view.getTag();
                Bundle bundle = new Bundle();
                bundle.putParcelable(AppConstants.EXTRA_STORE, store);
                bundle.putString(AppConstants.EXTRA_CATEGORY_NAME, "aa");
                ((BaseActivity) getActivity()).replaceFragment(null, new StoreDetailFragment(), bundle, true, R.id.contentFrameMe);
                break;
            default:
                super.onClick(view);
        }
    }

    private void showVisited() {
        ((TextView) mView.findViewById(R.id.txtVisited)).setBackgroundColor(getResources().getColor(R.color.blue_3));
        ((TextView) mView.findViewById(R.id.txtVisited)).setTextColor(Color.WHITE);
        ((TextView) mView.findViewById(R.id.txtBought)).setBackgroundResource(R.drawable.outline_green);
        ((TextView) mView.findViewById(R.id.txtBought)).setTextColor(getResources().getColor(R.color.blue_3));
    }

    private void showBought() {
        ((TextView) mView.findViewById(R.id.txtBought)).setBackgroundColor(getResources().getColor(R.color.blue_3));
        ((TextView) mView.findViewById(R.id.txtBought)).setTextColor(Color.WHITE);
        ((TextView) mView.findViewById(R.id.txtVisited)).setBackgroundResource(R.drawable.outline_green);
        ((TextView) mView.findViewById(R.id.txtVisited)).setTextColor(getResources().getColor(R.color.blue_3));
    }


}
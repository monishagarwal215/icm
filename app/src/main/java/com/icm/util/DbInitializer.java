package com.icm.util;

import android.os.AsyncTask;

import com.icm.constants.AppDatabase;

/**
 * Created by monish on 07/06/17.
 */

public class DbInitializer {

    public static void populateDbAsynTask(final AppDatabase db) {
        PopulateDbAsync task = new PopulateDbAsync(db);
        task.execute();
    }


    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final AppDatabase mDb;

        PopulateDbAsync(AppDatabase db) {
            mDb = db;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            populateWithTestData(mDb);
            return null;
        }
    }


    private static void populateWithTestData(AppDatabase mDb) {


    }

}

package com.icm.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.icm.constants.AppConstants;
import com.icm.model.request.Category;
import com.krapps.application.BaseApplication;
import com.krapps.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;


public class LookAtMePrefernece {

    private static SharedPreferences mPreferences;
    private static LookAtMePrefernece mInstance;
    private static Editor mEditor;

    private static String IS_LOGGED_IN = "is_logged_in_new";
    private static String APP_VERSION = "app_version";
    private static String CATEGORY_WITH_STORE = "category_with_store";


    private LookAtMePrefernece() {
    }

    public static LookAtMePrefernece getInstance() {
        if (mInstance == null) {
            Context context = BaseApplication.mContext;
            mInstance = new LookAtMePrefernece();
            mPreferences = context.getSharedPreferences(AppConstants.NIGHT_ADVISOR_PREFS, Context.MODE_PRIVATE);
            mEditor = mPreferences.edit();
        }
        return mInstance;
    }

    public void saveCategories(List<Category> value) {
        mEditor.putString(CATEGORY_WITH_STORE, new Gson().toJson(value)).apply();
    }

    public ArrayList<Category> getCategories() {
        Gson gson = new Gson();
        String contactListString = mPreferences.getString(CATEGORY_WITH_STORE, null);
        if (!StringUtils.isNullOrEmpty(contactListString)) {
            ArrayList<Category> categories = gson.fromJson(contactListString, new TypeToken<List<Category>>() {
            }.getType());
            return categories;
        }
        return null;
    }


}
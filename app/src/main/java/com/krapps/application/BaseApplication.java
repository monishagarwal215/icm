package com.krapps.application;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.Handler;

import com.icm.constants.AppConstants;
import com.icm.constants.AppDatabase;
import com.krapps.database.BaseSqliteOpenHelper;
import com.krapps.network.VolleyManager;

/**
 * @author Dinesh Adhikari
 */
public class BaseApplication extends Application {

    private static final String TAG = BaseApplication.class.getSimpleName();
    public static Handler mHandler = null;

    private static BaseSqliteOpenHelper dbHelper;
    public static Context mContext = null;
    private static AppDatabase appDatabase;


    @Override
    public void onCreate() {

        super.onCreate();
        mContext = getApplicationContext();
        mHandler = new Handler();

        getAppDatabase(this);
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

    }

    public VolleyManager getVolleyManagerInstance() {
        return VolleyManager.getInstance(getApplicationContext());
    }

    public static BaseSqliteOpenHelper getDbHelperInstance(Context context) {
        if (dbHelper == null) {
            dbHelper = new BaseSqliteOpenHelper(context);
        }
        return dbHelper;
    }

    public static AppDatabase getAppDatabase(Context context) {
        if (appDatabase == null) {
            appDatabase = Room.databaseBuilder(context, AppDatabase.class, AppConstants.DB_NAME).
                    allowMainThreadQueries()
                    .build();
        }
        return appDatabase;
    }

    @Override
    public void onTerminate() {
        dbHelper.close();
        super.onTerminate();
    }

}

package com.krapps.network;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.JsonSyntaxException;
import com.krapps.listener.UpdateListener;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Custom request for multipart file upload using volley
 *
 * @author kapil.vij
 */
public class VolleyMultipartRequest<T> extends Request<T> {

    private static final String TAG = "VolleyMultipartRequest";
    private static final String PARAM_FILE_BODY = "photo";
    private UpdateListener updateListener;
    private File file;
    private MultipartEntity mHttpEntity;
    private HashMap<String, String> params;

    public VolleyMultipartRequest(String url, UpdateListener updateListener, File file, HashMap<String, String> params) {
        super(Method.POST, url, updateListener);
        this.updateListener = updateListener;
        this.file = file;
        this.params = params;
        mHttpEntity = buildMultipartEntity();
    }

	/*
     * private HttpEntity buildMultipartEntity() { MultipartEntityBuilder
	 * builder = MultipartEntityBuilder.create(); String fileName =
	 * file.getName(); builder.addBinaryBody(KEY_PICTURE, file,
	 * ContentType.create("image/jpeg"), fileName); return builder.build(); }
	 */

    private MultipartEntity buildMultipartEntity() {
        mHttpEntity = new MultipartEntity();
        Set<Map.Entry<String, String>> entrySet = params.entrySet();
        for (Map.Entry<String, String> entr : entrySet) {
            try {
                mHttpEntity.addPart(entr.getKey(), new StringBody(entr.getValue()));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        if (file != null) {
            mHttpEntity.addPart(PARAM_FILE_BODY, new FileBody(file));
        }
        /*
         * else { mHttpEntity.addPart(ApiConstants.PARAM_PROFILE_IMAGE, new
         * StringBody("image")); }
         */

        Log.i(TAG, mHttpEntity.toString());

        return mHttpEntity;
    }

    @Override
    public String getBodyContentType() {
        return mHttpEntity.getContentType().getValue();
    }

    @Override
    public int compareTo(Request<T> other) {
        return super.compareTo(other);
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            mHttpEntity.writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

    @Override
    protected Response parseNetworkResponse(NetworkResponse response) {
        // return Response.success("Uploaded", getCacheEntry());
        String json;
        try {
            json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, String.format("Encoding problem parsing API response. NetworkResponse:%s", response.toString()), e);
            return Response.error(new ParseError(e));
        }
        try {
            return Response.success(json, HttpHeaderParser.parseCacheHeaders(response));
        } catch (JsonSyntaxException e) {
            Log.e(TAG, String.format("Couldn't API parse JSON response. NetworkResponse:%s", response.toString()), e);
            Log.e(TAG, String.format("Couldn't API parse JSON response. Json dump: %s", json));
            return Response.error(new ParseError(e));
        }
    }

    /*@Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> params = new HashMap<String, String>();
        params.put("Content-Type", null);
        params.put("Accept", null);
        //params.put("X-LAM-OAuth", ApiConstants.CLIENT_SECRET);*//*
        *//*if (!StringUtils.isNullOrEmpty(LookAtMePrefernece.getInstance().getLamToken())) {
            params.put("LAM-OAUTH-TOKEN", LookAtMePrefernece.getInstance().getLamToken());
        }*//*
        return params;
    }*/

    @Override
    protected void deliverResponse(Object response) {
        updateListener.onResponse((String) response);
    }


}

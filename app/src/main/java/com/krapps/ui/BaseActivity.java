package com.krapps.ui;

import android.accounts.Account;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.LifecycleActivity;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.listener.UpdateListener.onUpdateViewListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import io.fabric.sdk.android.Fabric;


public class BaseActivity extends LifecycleActivity implements onUpdateViewListener, OnClickListener, UpdateJsonListener.onUpdateViewJsonListener {

    private static AlertDialog alertDialog;
    private BaseApplication mChappApplication;
    protected Account account;
    private ProgressDialog mProgressDialog;

    private LayoutInflater mInflater;
    private Picasso picasso;

    int imgSize = 0;


    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        Fabric.with(this, new Crashlytics());
        mInflater = LayoutInflater.from(getBaseContext());
        mChappApplication = (BaseApplication) getApplication();

        picasso = Picasso.with(this);
        //picasso.setLoggingEnabled(true);
        //picasso.setIndicatorsEnabled(true);
    }


    public void showProgressDialog(String... args) {
        if (isFinishing()) {
            return;
        }
        try {
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mProgressDialog.setCancelable(false);
            }
            if (args != null && args.length > 0) {
                mProgressDialog.setMessage(args[0]);
            } else {
                mProgressDialog.setMessage("Loading...");
            }
            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Removes the progress dialog
     */
    public void removeProgressDialog() {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.hide();
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        if (showLoader) {
            showProgressDialog();
        }
        String url = "";
        switch (reqType) {
            default:
                url = "";
                className = null;
                break;
        }
    }


    private String getJsonRequest(int reqType) {
        Gson gson = new Gson();
        return "";
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();

        return params;
    }


    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {

    }


    public void replaceFragment(String currentFragmentTag, Fragment fragment, Bundle bundle, boolean isAddToBackStack, int frameId) {
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        String tag = fragment.getClass().getSimpleName();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragmentLocal = getSupportFragmentManager().findFragmentById(frameId);
        if (fragmentLocal != null && fragmentLocal.getTag().equalsIgnoreCase(tag)) {
            ((BaseFragment) fragmentLocal).refreshFragment(bundle);
            return;
        }
        if (!StringUtils.isNullOrEmpty(currentFragmentTag)) {
            ft.add(frameId, fragment, tag);
            Fragment fragmentToHide = getSupportFragmentManager().findFragmentByTag(currentFragmentTag);
            if (fragmentToHide != null) {
                ft.hide(fragmentToHide);
            }
        } else {
            ft.replace(frameId, fragment, tag);
        }

        fragment.setRetainInstance(true);
        if (isAddToBackStack) {
            ft.addToBackStack(tag);
        }
        try {
            ft.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            ft.commitAllowingStateLoss();
        }
    }

    public int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }


    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void loadParseFileInBackground(String url, final ImageView imgView) {
        if (!StringUtils.isNullOrEmpty(url)) {
            picasso.load(url).centerCrop().fit().into(imgView);
        }
    }

    public void loadParseFileWithoutCrop(String url, final ImageView imgView) {
        if (!StringUtils.isNullOrEmpty(url)) {
            picasso.load(url).into(imgView);
        }
    }


    @Override
    public void updateView(String responseString, boolean isSuccess, int reqType) {

    }

    @Override
    public void onClick(View v) {

    }
}

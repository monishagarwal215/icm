package com.krapps.ui;

import android.app.Activity;
import android.arch.lifecycle.LifecycleFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;

import com.krapps.listener.UpdateListener.onUpdateViewListener;

public class BaseFragment extends LifecycleFragment implements OnClickListener, onUpdateViewListener {
    private String frgamentName;
    public BaseActivity mActivity;
    private float density;


    public BaseFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        density = mActivity.getResources().getDisplayMetrics().density;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        hideKeyboard();
    }

    public float getDensity() {
        return density;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = (BaseActivity) activity;
    }


    private void hideKeyboard() {
        // Check if no view has focus:
        View view = mActivity.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void setFragmentName(String frgamentName) {
        this.frgamentName = frgamentName;
    }

    public String getFragmentName() {
        return frgamentName;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStop() {
        super.onStop();
        // ((DashboardActivity) mActivity).removeProgressView();
    }

    /**
     * Override this method when refresh page is required in your fragment
     *
     * @param bundle
     */
    protected void refreshFragment(Bundle bundle) {

    }

    public void updateView(Object object) {

    }

    @Override
    public void onClick(View view) {
    }
}
